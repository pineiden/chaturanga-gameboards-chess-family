use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::convert::TryFrom;
use strum::IntoEnumIterator; // 0.17.1
use strum_macros::EnumIter; // 0.17.1

pub trait IsIdentity {
	fn is_ident(&self, destiny:&Self) -> bool;
}


#[derive(Debug,Hash, EnumIter, Eq, PartialEq, Clone)]
pub enum Route {
	Diagonal,
	First,
	Second
}

pub struct RoutePoints {
	points: HashMap<Route, Vec<Point>>
}

impl RoutePoints {
	pub fn new()->RoutePoints {
		let mut route_points = HashMap::new();
		for route in  Route::iter() {
			route_points.insert(route, vec![]);
		}		
	    RoutePoints { points: route_points }
	}

	pub fn get_points(&self) -> HashMap<Route, Vec<Point>> {
		self.points.clone()
	}

	pub fn get_points_by_route(&self, route: &Route) -> &Vec<Point>{
		&self.points[route]
	}


	pub fn add(&mut self, route:&Route, points:&Vec<Point>) {
		/*
		This function has no check about the type of route with the
		points.
		This check must be before, on the build  of points
		 */
		match self.points.get_mut(&route)  {
			Some(mut route_points)=>{
				for point in points.iter(){
					route_points.push(point.clone());
				}
			},
			None =>{
				self.points.insert(route.clone(), points.to_vec());
			}
		}

	}
}

#[derive(Debug,Default,Clone,Eq,PartialEq)]
pub struct Point {
	x: u8,
	y: u8,	
}

impl Point {
	pub fn new(x:u8, y:u8) -> Point {
		Self {x,y}
	}

	fn get_x(&self) -> u8{
		self.x
	}

	fn get_y(&self) -> u8{
		self.y
	}


	pub fn step_by_step(
		&self, 
		destiny:&Point, 
		diagonal:bool) -> RoutePoints {
		if diagonal && self.is_ident(destiny){
			let difference = self.x.abs_diff(destiny.x);
			// if 0, vec is static, destiny == origin
			let mut sign_x:i16 = 1;
			if destiny.x < self.x {
				sign_x = -1;
			} 

			let mut sign_y:i16 = 1; 
			if destiny.y < self.y {
				sign_y = -1;
			} 

			let points = (0..=difference).map(|v| {
				Point {
					x:((self.x as i16) + sign_x*(v as i16)) as u8, 
					y:((self.y as i16) + sign_y*(v as i16)) as u8,
				}
			}
			).collect();

			let mut route_points = RoutePoints::new();
			route_points.add(&Route::Diagonal, &points);

			route_points
			
		} else {
			let difference_x = self.x.abs_diff(destiny.x);
			let difference_y = self.y.abs_diff(destiny.y);
			
			let mut sign_x:i16 = 1;
			if destiny.x < self.x {
				sign_x = -1;
			} 

			let mut sign_y:i16 = 1; 
			if destiny.y < self.y {
				sign_y = -1;
			} 

			let mut route_points = RoutePoints::new();

			let mut steps_a:Vec<Point> = vec![];

			let base_y = self.y;
			let mut base_x = self.x;
			for x in 0..=difference_x {
				let point = Point {
					x:((self.x as i16) + sign_x*(x as i16)) as u8, 
					y:base_y,
				};				
				base_x = point.x;
				steps_a.push(point);
			}

			for y in 0..=difference_y {
				if y==0 {continue;}
				let point = Point {
					x:base_x, 
					y:((self.y as i16) + sign_y*(y as i16)) as u8,
				};				
				steps_a.push(point);
			}

			
			route_points.add(&Route::First, &steps_a);

			let mut steps_b:Vec<Point> = vec![];

			let mut base_y = self.y;
			let base_x = self.x;
			for y in 0..=difference_y {
				let point = Point {
					x:base_x, 
					y:((self.y as i16) + sign_y*(y as i16)) as u8,
				};				
				base_y = point.y;
				steps_b.push(point);
			}

			for x in 0..=difference_x {
				if x==0 {continue;}

				let point = Point {
					x:((self.x as i16) + sign_x*(x as i16)) as u8, 
					y:base_y,
				};				
				steps_b.push(point);
			}

			route_points.add(&Route::Second, &steps_b);

			route_points
		}
	} 


}

impl IsIdentity for Point {
	fn is_ident(&self, destiny:&Point) -> bool {
		let dest = destiny.clone();
		self.x.abs_diff(dest.get_x()) == self.y.abs_diff(dest.get_y())
	}
}

trait IsSide {}

trait HasSide {
	fn side(&self) -> Box<dyn IsSide>;
}


trait HasValue {
	fn value(&self) -> u8;
}

trait HasMovement {
	fn movement(&self) -> String;
	fn valid(&self, destiny:&Point)->bool;
}

trait CanJump {
	fn can_jump(&self) -> bool;
}

pub trait HasBoardlimit {
	fn haslimit(&self) -> bool;
}


pub struct Event {
	message: String
}

pub struct MovementLog {
	turn: u32,
	position: Point,
	empty_cell: bool,
	win: bool,
	mate:bool,
	check_mate: bool,
	message: Event
}

/*
This struct define a type of movement that could do a piece in a
specific moment, the moment is the Status
*/
pub struct Movement {
	name:String,
	_cx: i16,
	_cy: i16,
	//self_limit: u16, // limit to amount movements by turn
	_board_limit: bool, // if must be only inside board
	_diagonal: bool, // for bishop
	_can_turn: bool, //can turn movement to another direction 
	_can_convert:bool, // can be converted in another
	_can_mount:bool, // can be converted in another
	self_limit_turn: u16, //limit amount turno to another dir
}


impl Movement {
	pub fn new(
		name:&str,
		cx: i16, 
		cy: i16, 
		//self_limit:u16, 
		board_limit:bool,
		diagonal:bool,
		can_turn:bool,
		can_convert:bool,
		can_mount:bool,
		self_limit_turn:i32
) -> Movement {
		let mut lturn = 0;
		if self_limit_turn<0 {
			lturn = u16::MAX;
		} else {
			lturn = self_limit_turn as u16;
		}
		Movement {
			name:name.to_string(),
			_cx:cx,
			_cy:cy,
			//self_limit,
			_board_limit:board_limit, 
			_diagonal:diagonal, 
			_can_turn:can_turn,
			_can_convert:can_convert,
			_can_mount:can_mount,
			self_limit_turn:lturn}
	}
	pub fn cx(&self) -> i16 {
		self._cx
	}

	pub fn cy(&self) -> i16 {
		self._cy
	}

	pub fn diagonal(&self) -> bool {
		self._diagonal
	}
	pub fn can_turn(&self) -> bool {
		self._can_turn
	}

	pub fn can_convert(&self) -> bool {
		self._can_convert
	}

	pub fn can_mount(&self) -> bool {
		self._can_mount
	}

	pub fn board_limit(&self) -> bool {
		self._board_limit
	}
}

impl HasBoardlimit for Movement {
	fn haslimit(&self) -> bool{
		self.board_limit()
	}	
}

#[derive(Debug, Hash, EnumIter,Eq,PartialEq, Clone)]
pub enum Status {
	Start = 0,
	Middle = 1,
	Battle = 2,
	Mount = 3,
	OnTheWay = 4,
	End = 5
}



impl TryFrom<usize>  for Status {
	type Error = ();

	fn try_from(value:usize) -> Result<Status, Self::Error> {
		match value {
			value if value==Status::Start as usize => {
				Ok(Status::Start)
			},
			value if value==Status::Middle as usize => {
				Ok(Status::Middle)
			},
			value if value==Status::Battle as usize => {
				Ok(Status::Battle)
			},
			value if value==Status::Mount as usize => {
				Ok(Status::Mount)
			},
			value if value==Status::OnTheWay as usize => {
				Ok(Status::OnTheWay)
			},
			value if value==Status::End as usize => {
				Ok(Status::End)
			},
			default => Err(())
		}
	}
}

/*
MovementSet as its name says, is a mapping of the status, it's all
types of movements that a piece can incurr, and mapped with a set of
valid movements
*/

pub struct MovementSet {
	_movements: HashMap<Status, Vec<Movement>>
}



impl MovementSet {
	pub fn new()->MovementSet {
		let mut map_movs = HashMap::new();
		for status in Status::iter() {
			map_movs.insert(status, vec![]);
		}
		MovementSet { _movements: map_movs }
	}

	pub fn add(&mut self, status:&Status, movement:Movement) {	
		match self._movements.get_mut(status)  {
			Some(movements)=>{
				movements.push(movement);
			},
			None =>{
				self._movements.insert(status.clone(), vec![movement]);
			}
		}
	}

	pub fn drop(&mut self, status:&Status, index:usize) {
		match self._movements.get_mut(status)  {
			Some(movements)=>{
				movements.remove(index);
			},
			None =>{}
		}
	}

	pub fn movements(&self) -> &HashMap<Status,Vec<Movement>> {
		&self._movements
	}

	pub fn get(&self, index:&usize) -> Option<&Vec<Movement>> {
		// TODO: handle panic
		self._movements.get(&Status::try_from(index.clone()).unwrap())
	}

}


/*
Simple gameboard::

- Two sides
*/

#[derive(Debug,Clone)]
pub enum Side {
	White,
	Black
}

impl IsSide for Side {}

pub struct BasePiece {
	position:Point,
	name:String,
	color:Side,
	value:u8,
	status: Status,
	movements:MovementSet,
	//log: MovementLog // register of movements 
}

impl BasePiece {
	pub fn new(
		position:Point,
		name:String,
		color:Side,
		value:u8,
		status: Status,
		movements:MovementSet,
		//log: MovementLog // register of movements 
	) -> Self {
		Self {
		   position,
		   name,
		   color,
		   value,
		   status,
		   movements,
		  // log

		}

	}
	fn status(&self)->Status {
		self.status.clone()
	}
}

impl HasSide for BasePiece{
	fn side(&self) -> Box<dyn IsSide> {
		Box::new(self.color.clone())
	}
}

impl HasValue for BasePiece {
	fn value(&self) -> u8 {
		self.value
	}
}

impl CanJump for BasePiece {
	fn can_jump(&self) -> bool {
		false
	}
}


impl HasMovement for BasePiece {
	fn movement(&self) -> String {
		String::default()
} 
	fn valid(&self, destiny:&Point)->bool  {
		/*
		Check movement of this piece to destiny given properties of
		status and movements
		 */
		true
	}
}
