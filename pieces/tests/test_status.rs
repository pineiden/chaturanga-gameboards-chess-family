use pieces::{Status};

#[test]
fn test_status_from() {
	assert_eq!(Status::try_from(0).unwrap(), Status::Start);
	assert_eq!(Status::try_from(1).unwrap(), Status::Middle);
	assert_eq!(Status::try_from(2).unwrap(), Status::Battle);
	assert_eq!(Status::try_from(3).unwrap(), Status::Mount);
	assert_eq!(Status::try_from(4).unwrap(), Status::OnTheWay);
	assert_eq!(Status::try_from(5).unwrap(), Status::End);
}

#[test]
#[should_panic]
fn test_status_from_mustfail() {
	assert_eq!(Status::try_from(100).unwrap(), Status::Start);
	assert_eq!(Status::try_from(200).unwrap(), Status::Start);
}
