use pieces::{Route, RoutePoints, Point};

#[test]
fn test_route_new_is_empty() {
	let route_points = RoutePoints::new();
	let points = route_points.get_points();
	for (key, val) in points.iter() {
		assert!(val.is_empty());
	}
}

#[test]
fn test_add_points_to_route() {
	let points = vec![
		 Point::new(0,0),
		 Point::new(5,5), //
		 Point::new(1,2), //
		 Point::new(6,7), //
		 Point::new(0,7), //
		 Point::new(7,0), //
		 Point::new(6,4), //
		 Point::new(3,1), //
		];
	let route = Route::Diagonal;
	let mut route_points = RoutePoints::new();
	route_points.add(&route, &points);
	let points_check = route_points.get_points_by_route(&route);
	assert_eq!(&points, points_check);
}
