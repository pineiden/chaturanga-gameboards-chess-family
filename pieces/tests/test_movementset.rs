use pieces::{MovementSet, Movement, Status};
/*
		name:&str,
		cx: u16, 
		cy: u16, 
		self_limit:u16, 
		board_limit:bool,
		diagonal:bool,
		can_turn:bool,
		self_limit_turn:u16

*/

// check given string to String belongs to another direction of memory
// check cx, cy could be 0,1, to N-> inf u16::MAX, if cx or cy =-1,
// set to inf
#[test]
fn test_cxcy_definition(){
	let cx = 10;
	let cy = 0;
	let mov = Movement::new("test", cx, cy,  true, true, true, true, true,10); 
	assert_eq!(mov.cx(), cx);
	assert_eq!(mov.cy(), cy);
}

#[test]
fn test_cxcy_definition_inf(){
	let cx = i16::MIN;
	let cy = 0;
	let mov = Movement::new("test", cx, cy, true, true, true, true, true,10); 
	assert_eq!(mov.cx(), i16::MIN);
	assert_eq!(mov.cy(), cy);
	let cx = 10;
	let cy = i16::MAX;
	let mov = Movement::new("test", cx, cy, true, true, true, true, true,10); 
	assert_eq!(mov.cx(), cx);
	assert_eq!(mov.cy(), i16::MAX);
	let cx = i16::MAX;
	let cy = i16::MIN;
	let mov = Movement::new("test", cx, cy,  true, true, true, true, true,10); 
	assert_eq!(mov.cx(), i16::MAX);
	assert_eq!(mov.cy(), i16::MIN);

}

#[test]
fn test_movementset_new_empty() {
	let ms = MovementSet::new();
	for (k,v) in ms.movements().iter(){ 
		assert_eq!(v.len(), 0);
	}
}

#[test]
fn test_movementset_with_movements() {
	let mut ms = MovementSet::new();
	let lens = [2,1,3,0,3,1];
	// add one set
	// check status Start
	// describe start movement pawn (peón al inicio)
	let cx = 0;
	let cy = 1;
	let mov1 = Movement::new("test", cx, cy,  true, false, false,false, false, 10); 
	let cx = 0;
	let cy = 2;
	let mov2 = Movement::new("test", cx, cy,  true, false, false,false, false, 10); 
	
	ms.add(&Status::Start, mov1);
	ms.add(&Status::Start, mov2);

	// add another set
	// check Middle
	let cx = 0;
	let cy = i16::MAX;
	let mov = Movement::new("test", cx, cy,  true, false, false,false,
							false, 10); 

	ms.add(&Status::Middle, mov);

	// add another set
	// check Battler
	let cx = 1;
	let cy = 1;
	let mov1 = Movement::new("eat to right", cx, cy,  true, true, false,
							 false, false, 10); 
	let cx = -1;
	let cy = 1;
	let mov2 = Movement::new("eat to left", cx, cy,  true, true, false,
							 false, false, 10); 

	let cx = 0;
	let cy = 1;
	let mov3 = Movement::new("continue", cx, cy,  
							 true, false, false, false, false,10); 


	ms.add(&Status::Battle, mov1);
	ms.add(&Status::Battle, mov2);
	ms.add(&Status::Battle, mov3);

	//check all MOUNT
	// empty -> OK

	// pawn on the way
	let cx = 1;
	let cy = 1;
	let mov1 = Movement::new("eat to right", cx, cy,  
							 true, true, false, false, false,10); 
	let cx = 0;
	let cy = 1;
	let mov2 = Movement::new("continue", cx, cy,  
							 true, false, false, false, false,10); 
	let cx = -1;
	let cy = 1;
	let mov3 = Movement::new("eat to left", cx, cy,  
							 true, true, false, false, false, 10); 
	ms.add(&Status::OnTheWay, mov1);
	ms.add(&Status::OnTheWay, mov2);
	ms.add(&Status::OnTheWay, mov3);


	// END
	let cx = 0;
	let cy = 1;
	let mov = Movement::new("end and convert", cx, cy,  true, true, true, true, true,10); 
	ms.add(&Status::End, mov);

	for i in 0..=5 {
		let v = ms.get(&i).unwrap();
		let amount  = lens[i] as usize;
		assert_eq!(v.len(), amount);
	}	
}


#[test]
fn test_movementset_drop_status_mov() {
	// delete some movement from a status
}
