use pieces::{Point,IsIdentity,Route,RoutePoints};

#[test]
fn test_point_diagonal_ident() {
	/*
	This test check all cuadrants, and get_x, get_y fns
	 */
	let p1 = Point::new(0,0); //
	let p2 = Point::new(5,5); //
	assert!(p1.is_ident(&p2));

	let p3 = Point::new(1,2); //
	let p4 = Point::new(6,7); //
	assert!(p3.is_ident(&p4));

	let p5 = Point::new(0,7); //
	let p6 = Point::new(7,0); //
	assert!(p5.is_ident(&p6));

	let p7 = Point::new(6,4); //
	let p8 = Point::new(3,1); //
	assert!(p7.is_ident(&p8));

}


#[test]
fn test_check_path_to(){
	/*
	This test check the correct path to related to if a diagonal path
	can be done
	 */
	let origin = Point::new(6,4); //
	let destiny = Point::new(3,1); //
	let route_points_nodiag = origin.step_by_step(&destiny, false);
	let route_points_diag = origin.step_by_step(&destiny, true);

	let route_diag_path =  route_points_diag
		.get_points_by_route(&Route::Diagonal);

	let route_first_path =  route_points_nodiag
		.get_points_by_route(&Route::First);

	let route_second_path =  route_points_nodiag
		.get_points_by_route(&Route::Second);

	let diag_path = vec![
		Point::new(6,4),
		Point::new(5,3),
		Point::new(4,2),
		Point::new(3,1),
	];

	let first_path = vec![
		Point::new(6,4),
		Point::new(5,4),
		Point::new(4,4),
		Point::new(3,4),
		Point::new(3,3),
		Point::new(3,2),
		Point::new(3,1),
	];

	let second_path = vec![
		Point::new(6,4),
		Point::new(6,3),
		Point::new(6,2),
		Point::new(6,1),
		Point::new(5,1),
		Point::new(4,1),
		Point::new(3,1),
	];

	assert_eq!(&diag_path, route_diag_path);
	assert_eq!(&first_path, route_first_path);
	assert_eq!(&second_path, route_second_path);

}

