use pieces::{HasSide, HasValue, HasMovement, CanJump, Point};



/*
this create an instance of board game with some generic type 
tha must have in common a trait that allows share same space
despite the posible different behaviour
*/
struct Board<Piece:HasSide+HasValue+HasMovement+CanJump> {
	horizontal: u8,
	vertical: u8,	
	cells: Vec<Vec<Option<Piece>>>
}

impl Board<T> {
	fn new(horizontal: u8, vertical: u8) {
		let matrix = vec![Option::None; horizontal*vertical]
		Board { horizontal: horizontal, vertical: vertical, cells:matrix}
	}

	fn place(&mut self,piece:&T, destiny:&Point) -> bool {
		if self.cells[destiny.x][destiny.y].is_none() {
			self.cells[destiny.x][destiny.y] = piece;
			true
		} else {
			false
		}
	}

	fn move_piece(&mut self, origin:&Point, destiny:&Point) -> bool {
		let has_piece = self.cells[origin.x][origin.y].is_some();
		let piece = self.cells[origin.x][origin.y].unwrap_or(Option::None);

		if has_piece && self.place(piece, destiny) {
			self.cells[origin.x][origin.y] = Option::None;
			self.cells[destiny.x][destiny.y] = piece;
			true
		} else {
			false
		}
	}

	fn power(&self, side:&Side) -> u32 {
		let counter:u32 = 0;
		for row in self.cells.iter() {
			for cell in row.iter() {
				if cell.is_some() {
					let piece = self.cell.expect("Some piece");
					if piece.side() == side {
						counter += 1*piece.value();
					}
				}
			}
		}
		counter 
	}

	fn limits(&self) -> (u8,u8) {
		(self.horizontal, self.vertical)
	}


}
